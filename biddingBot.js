import * as dotenv from "dotenv"
import { ethers } from "ethers"
import { apiClient } from "./src/apiClient.js"
dotenv.config()

import { buildCollectionOffer, buildItemOffer } from "./src/buildOffer.js"
import { getNetwork } from "./src/network.js"
import { postItemOffer, postCriteriaOffer } from "./src/postOffer.js"
import { signOffer } from "./src/signOffer.js"

const network = getNetwork()

async function main() {
  const collection = process.env.MAINNET_ITEM_ASSET_CONTRACT_ADDRESS
  const offerPrice = ethers.utils.parseEther(process.env.OFFER_PRICE).toString()

  try {
    const response = await apiClient.get(`v1/asset_contract/${collection}`)

    const slug = response.data.collection.slug

    const quantity = Number(process.env.OFFER_QUANTITY) || 1

    const collectionOffer = await buildCollectionOffer({
      collectionSlug: slug,
      quantity: quantity,
      priceWei: BigInt(offerPrice),
      expirationSeconds: BigInt(901),
    })

    const collectionSignature = await signOffer(collectionOffer)

    const collectionResponse = await postCriteriaOffer(
      slug,
      collectionOffer,
      collectionSignature,
    )

    console.log(
      `Collection offer posted! Order Hash: ${collectionResponse.order_hash}`,
    )

    const itemOffer = await buildItemOffer({
      assetContractAddress: network.itemAssetContractAddress,
      tokenId: network.itemTokenIdentifier,
      quantity: 1,
      priceWei: BigInt("3130000000000000"),
      expirationSeconds: BigInt(901),
    })
    const itemSignature = await signOffer(itemOffer)
    const itemResponse = await postItemOffer(itemOffer, itemSignature)
    const itemOrderHash = itemResponse.order.order_hash
    console.log(`Item offer posted! Order Hash: ${itemOrderHash}`)
  } catch (error) {
    console.log("something went wrong")
  }
}

main().catch(error => console.error(error))
