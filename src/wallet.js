import { Wallet } from "ethers"
import * as dotenv from "dotenv"

dotenv.config()
export const getWallet = () => {
  const privateKey = process.env.WALLET_PRIVATE_KEY
  if (privateKey === undefined) {
    throw new Error("No private key found in .env")
  }
  return new Wallet(privateKey)
}
