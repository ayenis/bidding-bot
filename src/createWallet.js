import { Wallet } from "ethers"
import * as dotenv from "dotenv"
dotenv.config()

const privateKey = "0x" + process.env.WALLET_PRIVATE_KEY

const wallet = new Wallet(privateKey)
console.log("Address: " + wallet.address)
