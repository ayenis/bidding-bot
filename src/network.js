import { Network } from "opensea-js"
import * as dotenv from "dotenv"
dotenv.config()

const networks = {
  mainnet: {
    chainId: 1,
    chainName: "ethereum",
    baseURL: "https://api.opensea.io/api/",
    wethAddress: "0xC02aaA39b223FE8D0A0e5C4F27eAD9083C756Cc2",
    apiKey: process.env.API_KEY,
    rpcUrl: process.env.MAINNET_RPC_URL,
    network: Network.Main,
    itemAssetContractAddress: process.env.MAINNET_ITEM_ASSET_CONTRACT_ADDRESS,
    itemTokenIdentifier: "1",
  },
  testnets: {
    chainId: 5,
    chainName: "goerli",
    baseURL: "https://testnets-api.opensea.io/api/",
    wethAddress: "0xB4FBF271143F4FBf7B91A5ded31805e42b2208d6",
    apiKey: undefined,
    rpcUrl:
      process.env.TESTNETS_RPC_URL ||
      "https://goerli.infura.io/v3/359c5140734c47e990cf43bb7052527b",
    network: Network.Goerli,
    itemAssetContractAddress:
      process.env.TESTNETS_ITEM_ASSET_CONTRACT_ADDRESS ||
      "0xd0a21d074efc3393853accd481160a070a20cf32",
    itemTokenIdentifier: process.env.TESTNETS_ITEM_TOKEN_IDENTIFIER || "143",
  },
}

export const getNetwork = () => {
  const network = process.env.NETWORK
  switch (network) {
    case "mainnet":
      return networks.mainnet
    case "testnets":
      return networks.testnets
    case undefined:
      console.warn("No network found in .env. Defaulting to testnets.")
      return networks.testnets
    default:
      throw `Invalid network found in .env. Please add "NETWORK=mainnet" or "NETWORK=testnets" to your .env file.`
  }
}
