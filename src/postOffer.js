import { apiClient } from "./apiClient.js"
import { getNetwork } from "./network.js"

const getCriteria = collectionSlug => {
  return {
    collection: {
      slug: collectionSlug,
    },
  }
}

const network = getNetwork()

export const postCriteriaOffer = async (collectionSlug, offer, signature) => {
  const payload = {
    criteria: getCriteria(collectionSlug),
    protocol_data: {
      parameters: offer,
      signature,
    },
  }

  try {
    const response = await apiClient.post("v2/offers", payload)

    return response.data
  } catch (error) {
    console.log(error)
  }
}

export const postItemOffer = async (offer, signature) => {
  const payload = {
    parameters: offer,
    signature,
  }
  const response = await apiClient.post(
    `v2/orders/${network.chainName}/seaport/offers`,
    payload,
  )

  return response.data
}
