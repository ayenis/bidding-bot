import { apiClient, sdkClient } from "./apiClient.js"
import { getNetwork } from "./network.js"
import { getWallet } from "./wallet.js"

const network = getNetwork()

const conduitKey =
  "0x0000007b02230091a7ed01230072f7006a004d60a8d4e71d599b8104250f0000"

const getOfferer = () => {
  const wallet = getWallet()

  return wallet.address
}

const getOffer = priceWei => {
  return [
    {
      itemType: 1, // ERC 20
      token: network.wethAddress,
      identifierOrCriteria: 0,
      startAmount: priceWei.toString(),
      endAmount: priceWei.toString(),
    },
  ]
}

const getFee = (priceWei, feeBasisPoints, receipient) => {
  const fee = (priceWei * feeBasisPoints) / BigInt(10000)
  if (fee <= 0) {
    return null
  }
  return {
    itemType: 1, // ERC 20
    token: network.wethAddress,
    identifierOrCriteria: 0,
    startAmount: fee.toString(),
    endAmount: fee.toString(),
    recipient: receipient,
  }
}

const extractFeesApi = (feesObject, priceWei) => {
  const fees = []

  for (const [_category, categoryFees] of Object.entries(feesObject)) {
    for (const [address, basisPoints] of Object.entries(categoryFees)) {
      const fee = getFee(priceWei, BigInt(basisPoints), address)
      if (fee) {
        fees.push(fee)
      }
    }
  }

  return fees
}

const extractFeesSdk = (feesObject, priceWei) => {
  const fees = []
  const feesToAdd = [...feesObject.openseaFees, ...feesObject.sellerFees]

  for (const [address, basisPoints] of feesToAdd) {
    const fee = getFee(priceWei, BigInt(basisPoints), address)
    if (fee) {
      fees.push(fee)
    }
  }

  return fees
}

const getItemFees = async (assetContractAddress, tokenId, priceWei) => {
  const asset = await sdkClient.api.getAsset({
    tokenAddress: assetContractAddress,
    tokenId,
  })

  const fees = asset.collection.fees
  return extractFeesSdk(fees, priceWei)
}

const getCriteriaFees = async (collectionSlug, priceWei) => {
  const response = await apiClient.get(`v1/collection/${collectionSlug}`)

  const feesObject = response.data.collection.fees

  return extractFeesApi(feesObject, priceWei)
}

const getBuildData = async (collectionSlug, quantity) => {
  const offerer = getOfferer()
  const response = await apiClient.post("v2/offers/build", {
    offerer,
    quantity,
    criteria: {
      collection: {
        slug: collectionSlug,
      },
    },
  })

  return response.data.partialParameters
}

const getItemTokenConsideration = async (
  assetContractAddress,
  tokenId,
  quantity,
) => {
  const offerer = getOfferer()
  return {
    itemType: 2,
    token: assetContractAddress,
    identifierOrCriteria: tokenId,
    startAmount: quantity,
    endAmount: quantity,
    recipient: offerer,
  }
}

const getCriteriaConsideration = async (
  criteriaFees,
  collectionSlug,
  priceWei,
) => {
  const fees = [
    ...criteriaFees,
    ...(await getCriteriaFees(collectionSlug, priceWei)),
  ]

  return fees.filter(fee => fee !== null)
}

const getItemConsideration = async (
  assetContractAddress,
  tokenId,
  quantity,
  priceWei,
) => {
  const fees = [
    await getItemTokenConsideration(assetContractAddress, tokenId, quantity),
    ...(await getItemFees(assetContractAddress, tokenId, priceWei)),
  ]

  return fees
}

const getSalt = () => {
  return Math.floor(Math.random() * 100_000).toString()
}

export const buildCollectionOffer = async offerSpecification => {
  const { collectionSlug, quantity, priceWei, expirationSeconds } =
    offerSpecification

  const now = BigInt(Math.floor(Date.now() / 1000))
  const startTime = now.toString()
  const endTime = (now + expirationSeconds).toString()
  const buildData = await getBuildData(collectionSlug, quantity)
  const consideration = await getCriteriaConsideration(
    buildData.consideration,
    collectionSlug,
    priceWei,
  )

  const offer = {
    offerer: getOfferer(),
    offer: getOffer(priceWei),
    consideration,
    startTime,
    endTime,
    orderType: 2,
    zone: buildData.zone,
    zoneHash: buildData.zoneHash,
    salt: getSalt(),
    conduitKey,
    totalOriginalConsiderationItems: consideration.length.toString(),
    counter: 0,
  }

  return offer
}

export const buildItemOffer = async offerSpecification => {
  const {
    assetContractAddress,
    tokenId,
    quantity,
    priceWei,
    expirationSeconds,
  } = offerSpecification

  const now = BigInt(Math.floor(Date.now() / 1000))
  const startTime = now.toString()
  const endTime = (now + expirationSeconds).toString()
  const consideration = await getItemConsideration(
    assetContractAddress,
    tokenId,
    quantity,
    priceWei,
  )

  const offer = {
    offerer: getOfferer(),
    offer: getOffer(priceWei),
    consideration,
    startTime,
    endTime,
    orderType: 0,
    zone: "0x0000000000000000000000000000000000000000",
    zoneHash:
      "0x0000000000000000000000000000000000000000000000000000000000000000",
    salt: getSalt(),
    conduitKey,
    totalOriginalConsiderationItems: consideration.length.toString(),
    counter: 0,
  }

  return offer
}
