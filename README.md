# Project Setup

This bidding bot places collection offer and item offers using opensea and seaport api

## Installation

Use the package manager [yarn](https://yarnpkg.com/) to install project dependencies.

```bash
yarn install
```

## Environment Variables

```bash
cp .env.example .env
```
Replace all env variables appropriately

## Run

```bash
yarn bidding-bot
```

## License

[MIT](https://choosealicense.com/licenses/mit/)